# Activity

# 1. Accept a year input from the user and determine if it is a leap year or not

# Add validation
# a. strings are not allowed for inputs
# b. no zero or negative values


year = 0

while True:
	try:
		year = int(input("Enter a year to determine if it is a leap year or not.\n"))
	
	except ValueError:
		print("Please enter a valid year (an integer greater than zero).")
		continue

	if year > 0:

		if (year % 400 == 0) and (year % 100 == 0):
			print (f"{year} is a leap year.")
		elif (year % 4 == 0) and (year % 100 != 0):
			print (f"{year} is a leap year.")
		else:
			print (f"{year} is not a leap year.")

		break

	else:
		print("Please enter a valid year (an integer greater than zero).")


# 2. Accept two numbers (row and col) from the user and create a grid of asteriks using two numbers (row and col)
# use two while loops


row = int(input("Enter number of rows.\n"))
col = int(input("Enter number of columns.\n"))

r = 0

while r < row:
	r += 1
	print("*" * col)