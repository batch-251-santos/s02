# [SECTION]  Taking input from user

# username = input("Please enter your name:\n")
# print(f"Hello {username}! Welcome to the Python Short Course.")


# num1 = int(input("Please input first number: \n"))
# num2 = int(input("Enter second number: \n"))

# print(f"The sum of {num1} and {num2} is {num1 + num2}.")

# [SECTION] If-else Statements

test_num = 75

if test_num >=  60:
	print("Test Passed.")
else:
	print("Test Failed.")


# test_num2 = int(input("Please enter the second test number. \n"))

# If-else statements don't have curly brackets but instead rely on the spacing of the indention to denote the scope of the statement. It also uses the colon 
# if test_num2 > 0:
# 	print("The number is positive.")
# elif test_num2 == 0:
# 	print("The number is zero.")
# else:
# 	print("The number is negative.")


# [SECTION] Loops
# While Loop
# i = 1
# while i <= 5:
# 	print(f"Current count {i}")
# 	i += 1

# For Loop
# fruits = ["apple", "banana", "cherry"]
# for fruit in fruits:
# 	print(fruit)


# array_of_numbers = [1, 2, 3, 4, 5]

# range() function

# 1st argument = where the range starts
# 2nd argument = where the range ends
# 3rd argument = increment

# for x in range(6):
#	print(f"The current value is {x}")

# for x in range(5, 10):
#	print(f"The current value is {x}")

# for x in range(5, 20, 2):
# 	print(f"The current value is {x}")


# [SECTION] Break and Continue Statement

# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k) 